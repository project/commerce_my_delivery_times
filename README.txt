Commerce My Delivery Times allows you to configure desired time slot. 
You can configure master time slot which will appear on checkout pane. 
You can also create an individual time slot which is different from Master
slot configuration which will appear on checkout time slot pane respectively. 
Edit or delete feature of slot configuration is also provided.
You can configure start time and end time of every slot. 
Maximum number of order to be placed in every slot and threshold time of 
accepting order.
Holiday can also be configured. User cannot choose an holiday date.
A configuration setting page is provided for maximum number of days in 
checkout pane, maximum number of slots per day and whether to allow
same day delivery or not.
