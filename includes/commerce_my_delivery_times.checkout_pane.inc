<?php

/**
 * @file
 * Module to implement Commerce delivery time slots.
 */

/**
 * Implements hook_form().
 */
function commerce_my_delivery_times_checkout_form($form, &$form_state, $checkout_pane, $order) {

  $no_of_forward_days = variable_get('no_of_forward_days', '5');
  $same_day_delivery = variable_get('same_day_delivery', 'no');
  $current_date = date("Y-m-d");
  $required_days = array();
  for ($i = 1; $i <= $no_of_forward_days; $i++) {
    if ($same_day_delivery == 'no') {
      $slot_current_date = date("Y-m-d", strtotime("+" . $i . " day", strtotime($current_date)));
    }
    else {
      $slot_current_date = date("Y-m-d", strtotime("+" . $i - 1 . " day", strtotime($current_date)));
    }
    $required_days[] = $slot_current_date;
  }
  $delivery_slot_display = array();
  foreach ($required_days as $i => $date) {
    $holiday_data = db_select('delivery_holiday_list', 'dhl')
            ->fields('dhl', array('holiday_date', 'holiday_reason'))
            ->condition('dhl.holiday_date', $date)
            ->execute()
            ->fetchAssoc();
    // If holiday date exist.
    if (!empty($holiday_data)) {
      $delivery_slot_display[$date][0] = $holiday_data['holiday_reason'];
    }
    else {
      $query = db_select('delivery_time_slots_master', 'dtsm');
      $query->join('delivery_time_slots', 'dts', 'dtsm.dtsm_id = dts.dtsm_id');
      $query->fields('dtsm');
      $query->fields('dts');
      $query->condition('dtsm.delivery_slot_date', $date);
      $delivery_slot_data = $query->execute()->fetchAll();
      if (empty($delivery_slot_data)) {
        $query = db_select('delivery_time_slots_master', 'dtsm');
        $query->join('delivery_time_slots', 'dts', 'dtsm.dtsm_id = dts.dtsm_id');
        $query->fields('dtsm');
        $query->fields('dts');
        $query->condition('dtsm.delivery_slot_date', 0);
        $master_delivery_slot_data = $query->execute()->fetchAll();

        foreach ($master_delivery_slot_data as $ds_data => $cp_master) {
          $delivery_slot_display[$date][$ds_data] = array(
            'dtsm_id' => $master_delivery_slot_data[$ds_data]->dtsm_id,
            'dts_id' => $master_delivery_slot_data[$ds_data]->dts_id,
            'delivery_slot_date' => $date,
            'slot_start_time' => $master_delivery_slot_data[$ds_data]->slot_start_time,
            'slot_end_time' => $master_delivery_slot_data[$ds_data]->slot_end_time,
            'slot_bucket_size' => $master_delivery_slot_data[$ds_data]->slot_bucket_size,
            'slot_threshold_time_accepting_order' => $master_delivery_slot_data[$ds_data]->slot_threshold_time_accepting_order,
          );
        }
      }
      else {
        foreach ($delivery_slot_data as $ds_data => $ds_regular) {
          $delivery_slot_display[$date][$ds_data] = array(
            'dtsm_id' => $delivery_slot_data[$ds_data]->dtsm_id,
            'dts_id' => $delivery_slot_data[$ds_data]->dts_id,
            'delivery_slot_date' => $date,
            'slot_start_time' => $delivery_slot_data[$ds_data]->slot_start_time,
            'slot_end_time' => $delivery_slot_data[$ds_data]->slot_end_time,
            'slot_bucket_size' => $delivery_slot_data[$ds_data]->slot_bucket_size,
            'slot_threshold_time_accepting_order' => $delivery_slot_data[$ds_data]->slot_threshold_time_accepting_order,
          );
        }
      }
    }
  }
  $max = array();
  foreach ($delivery_slot_display as $ds_data) {
    $max[] = count($ds_data);
  }
  $max_val = max($max);
  $date = date("Y-m-d");
  $delivery_slot_table = '';
  $delivery_slot_table .= '<div class="top-delivery-slot"><table class="table-bordered table-hover">';
  foreach ($delivery_slot_display as $ds_date => $ds_slot_data) {
    $count_temp = 0;
    $delivery_slot_table .= '<tr><td class="th">' . date("D, j M", strtotime($ds_date)) . '</td>';
    foreach ($ds_slot_data as $ds_val) {
      // Check for if holiday calendar.
      if (empty($ds_val['slot_start_time'])) {
        $delivery_slot_table .= '<td class="holiday"  align="center" colspan=' . $max_val . '>' . $ds_val . '</td>';
      }
      else {
        $slot_start_time = $ds_val['slot_start_time'];
        $slot_end_time = $ds_val['slot_end_time'];
        $count_temp++;
        $ds_val_for_id = $ds_val["dts_id"] . '_' . $ds_date . '_' . $slot_start_time . '_' . $slot_end_time . '_' . $ds_val['slot_bucket_size'];
        $nowtime = date("Y-m-d H:i");
        $delivery_slot_date = $ds_val['delivery_slot_date'];
        $slot_date_time = $delivery_slot_date . ' ' . $slot_start_time;
        $slot_threshold_time_accepting_order = $ds_val['slot_threshold_time_accepting_order'];
        $accepting_hours_min = explode('.', $slot_threshold_time_accepting_order);
        $accepting_hours = $accepting_hours_min[0];
        $accepting_min = $accepting_hours_min[1];
        $applicable_time = date('Y-m-d H:i', strtotime($slot_date_time . ' - ' . $accepting_hours . ' hours ' . $accepting_min . ' minute'));
        $remaining_buckets = $ds_val['slot_bucket_size'];

        // Check for entry in particular date field slot is created or not.
        $soft_commit_data_check = db_select('delivery_slot_soft_and_hard_commit', 'sc')
                ->fields('sc')
                ->condition('sc.delivery_time_slot_date', $delivery_slot_date)
                ->condition('sc.slot_start_time', $slot_start_time)
                ->execute()
                ->fetchAssoc();
        if (!empty($soft_commit_data_check)) {
          $slot_id = $soft_commit_data_check['slot_id'];
          $delivery_slot_table .= '<td><input id="' . $slot_id . '" class="cp_checkout" type="radio" name="cts_id_checkout" value="' . $ds_val_for_id . '"';
          if (isset($_SESSION["ds_time_slot_id"])) {
            if ($_SESSION["ds_time_slot_id"] == $slot_id) {
              $delivery_slot_table .= 'checked="checked"';
            }
          }
          if (($nowtime > $applicable_time) || ($soft_commit_data_check['hard_commit_count'] >= $soft_commit_data_check['slot_bucket_size'])) {
            $delivery_slot_table .= 'disabled';
          }
          $time_format = commerce_my_delivery_times_slot_time_format($slot_start_time, $slot_end_time);
          $remaining_size = $soft_commit_data_check['slot_bucket_size'] - $soft_commit_data_check['soft_commit_count'];
          $delivery_slot_table .= '> <label for="' . $soft_commit_data_check["slot_id"] . '">' . $time_format . '(<span class="cp_bucket" id="' . $soft_commit_data_check["slot_id"] . '_cp_bucket">' . $remaining_size . '</span> slots remaining)</label></td>';
        }
        else {
          $delivery_slot_table .= '<td><input id="' . $ds_val["dts_id"] . '-' . $ds_date . '" class="cp_checkout" type="radio" name="cts_id_checkout" value="' . $ds_val_for_id . '"';
          if (isset($_SESSION["ds_time_slot_id"])) {
            if ($_SESSION["ds_time_slot_id"] == $ds_val_for_id) {
              $delivery_slot_table .= 'checked="checked"';
            }
          }
          if (($nowtime > $applicable_time)) {
            $delivery_slot_table .= 'disabled';
          }
          $time_format = commerce_my_delivery_times_slot_time_format($slot_start_time, $slot_end_time);
          $delivery_slot_table .= '> <label for="' . $ds_val["dts_id"] . '-' . $ds_date . '">' . $time_format . '(<span class="cp_bucket" id="' . $ds_val["dts_id"] . '-' . $ds_date . '_cp_bucket">' . $remaining_buckets . '</span> slots remaining)</label></td>';
        }
      }
    }
    if (is_array($ds_slot_data[0])) {
      if ($count_temp < $max_val) {
        $diff = $max_val - $count_temp;
        for ($j = 0; $j < $diff; $j++) {
          $delivery_slot_table .= '<td align="center">--</td>';
        }
      }
    }
    $delivery_slot_table .= '</tr>';
  }
  $delivery_slot_table .= '</table></div>';

  $form_pane['checkout_delivery_slot'] = array(
    '#type' => 'markup',
    '#markup' => $delivery_slot_table,
    '#tree' => TRUE,
  );
  return $form_pane;
}

/**
 * Implements hook_validate().
 */
function commerce_my_delivery_times_checkout_form_validate(&$form, &$form_state) {

  if (empty($_SESSION['ds_time_slot_id'])) {
    form_set_error('checkout_delivery_slot', t('Please select preferred Delivery Time Slot'));
    return;
  }
  else {
    return TRUE;
  }
}

/**
 * Function returns time slot format on checkout pane.
 */
function commerce_my_delivery_times_slot_time_format($start_time, $end_time) {

  $delivery_start_time = date('g:i A', strtotime($start_time));
  $delivery_end_time = date('g:i A', strtotime($end_time));

  return $delivery_start_time . ' - ' . $delivery_end_time;
}
