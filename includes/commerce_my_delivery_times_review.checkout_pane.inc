<?php

/**
 * @file
 * Module to implement Commerce delivery time slots.
 */

/**
 * Implements hook_checkout_form().
 */
function commerce_my_delivery_times_review_checkout_form($form, &$form_state, $checkout_pane, $order) {

  $slot_id = $_SESSION["ds_time_slot_id"];
  $slot_data = db_select('delivery_slot_soft_and_hard_commit', 'sc')
    ->fields('sc',
      array('delivery_time_slot_date', 'slot_start_time', 'slot_end_time')
    )
    ->condition('sc.slot_id', $slot_id)
    ->execute()
    ->fetchAssoc();

  $delivery_time_slot_date = $slot_data['delivery_time_slot_date'];
  $delivery_start_time = date('g:i A', strtotime($slot_data['slot_start_time']));
  $delivery_end_time = date('g:i A', strtotime($slot_data['slot_end_time']));

  $form_pane['delivery_date'] = array(
    '#type' => 'markup',
    '#markup' => "Delivery Date : " . $delivery_time_slot_date,
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  $form_pane['delivery_timings'] = array(
    '#type' => 'markup',
    '#markup' => "Delivery Time : " . $delivery_start_time . ' to ' . $delivery_end_time,
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
  return $form_pane;
}

/**
 * Implements hook_checkout_form_submit().
 */
function commerce_my_delivery_times_review_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  $ds_time_slot_id = $_SESSION["ds_time_slot_id"];
  $order->field_commerce_dts_id[LANGUAGE_NONE][0]['value'] = $ds_time_slot_id;
  $hard_commit_count = db_select('delivery_slot_soft_and_hard_commit', 'sc')
    ->fields('sc', array('hard_commit_count'))
    ->condition('sc.slot_id', $ds_time_slot_id)
    ->execute()
    ->fetchField();

  $hard_commit_count++;

  db_update('delivery_slot_soft_and_hard_commit')
    ->fields(array('hard_commit_count' => $hard_commit_count))
    ->condition('slot_id', $ds_time_slot_id)
    ->execute();

  unset($_SESSION["ds_time_slot_id"]);
}
