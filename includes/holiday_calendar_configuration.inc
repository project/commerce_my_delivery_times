<?php

/**
 * @file
 * Module to implement Commerce delivery time slots.
 */

/**
 * Implements hook_form().
 */
function commerce_my_delivery_times_holiday_calendar_form($form, &$form_state) {

  $dateformat = 'm-d-Y';
  $timestamp = time();
  $dbv = $timestamp;
  $value = NULL;
  if (empty($dbv)) {
    $d = new DateTime('@' . $dbv);
    $value = $d->format($dateformat);
  }
  else {
    $d = new DateTime();
    $value = $d->format($dateformat);
  }
  $date = date('Y-m-d');
  $holiday_date = db_select('delivery_holiday_list', 'hl')
    ->fields('hl', array('holiday_date'))
    ->execute()
    ->fetchAll();

  $full_date = array();
  foreach ($holiday_date as $hdate) {
    $full_date[] = $hdate->holiday_date;
  }
  if (empty($full_date)) {
    $full_date = array('0' => $date);
  }
  drupal_add_js(drupal_get_path('module', 'commerce_my_delivery_times') . '/js/my_delivery_times.js');

  $settings = array(
    'date_data' => $full_date,
  );
  drupal_add_js(array('delivery_dates' => $settings), 'setting');

  $form['holiday_date'] = array(
    '#type' => 'date_popup',
    '#date_timezone' => date_default_timezone(),
    '#date_format' => $dateformat,
    '#date_year_range' => '0:+3',
    '#default_value' => $value,
    '#required' => TRUE,
  );

  $form['holiday_reason'] = array(
    '#title' => t('Holiday Reason'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Enter a reason for holiday'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'SUBMIT',
  );

  $header = array(
    array(
      'data' => 'Holiday Date',
      'field' => 'holiday_date',
      'sort' => 'asc',
    ),
    array('data' => 'Reason'),
    array('data' => 'Delete'),
  );

  $holiday_data = db_select('delivery_holiday_list', 'dhl')
    ->fields('dhl', array('hid', 'holiday_date', 'holiday_reason'))
    ->extend('PagerDefault')->limit(15)
    ->extend('TableSort')->orderByHeader($header)
    ->execute()
    ->fetchAll();

  $rows = array();
  foreach ($holiday_data as $holiday_list) {
    $row = array();
    $row[] = $holiday_list->holiday_date;
    $row[] = $holiday_list->holiday_reason;
    $row[] = l(t('Delete'), 'admin/commerce/delivery-times/delete-holiday/' . $holiday_list->hid, array('attributes' => array('onclick' => 'if(!confirm("Are you sure you want to delete holiday?")){return false;}')));
    $rows[] = $row;
  }
  if (!empty($rows)) {
    $output = "<br/><br/>Date : <b>" . $date . "</b>";
    $output .= theme('table',
      array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array('id' => 'sort-table'),
      ));
    $output .= theme('pager');

    $form['holiday_output_table'] = array(
      '#type' => 'markup',
      '#markup' => $output,
    );
  }
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function commerce_my_delivery_times_holiday_calendar_form_submit($form, &$form_state) {

  $holiday_reason = $form_state['values']['holiday_reason'];
  $holiday_date = $form_state['values']['holiday_date'];
  $date_check = db_select('delivery_holiday_list', 'hl')
    ->fields('hl', array('hid'))
    ->condition('hl.holiday_date', $holiday_date)
    ->execute()
    ->fetchfield();

  if (empty($date_check)) {
    db_insert('delivery_holiday_list')
      ->fields(array(
        'holiday_date' => $holiday_date,
        'holiday_reason' => $holiday_reason,
      ))
      ->execute();
    drupal_set_message(t("Holiday date added successfully"));
  }
  else {
    drupal_set_message(t("Holiday is already added"), "error");
  }
}

/**
 * Function to delete holiday date.
 */
function commerce_my_delivery_times_delete_holiday_date($hid) {

  db_delete('delivery_holiday_list')->condition('hid', $hid)->execute();
  drupal_set_message(t("Holiday date has been deleted Successfully."));
  drupal_goto("admin/commerce/delivery-times/holiday-configuration");
}
